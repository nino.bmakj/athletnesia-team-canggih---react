import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import NavBar from './Common/NavBar';
import Footer from './Common/Footer';
import Setting from './Common/Setting';
import UserTab from './Pages/UserTab';
import AthleteAll from './Pages/Athlete/AthleteAll';
import AthleteDetail from './Pages/Athlete/AthleteDetail';
import AthleteForm from './Pages/Athlete/AthleteForm';
import ClubAll from './Pages/Club/ClubAll';
import ClubDetail from './Pages/Club/ClubDetail';
import ClubForm from './Pages/Club/ClubForm';
import ScholarshipForm from './Pages/Scholarship/ScholarshipForm';
import ScholarshipDetail from './Pages/Scholarship/ScholarshipDetail';
import ScholarshipAll from './Pages/Scholarship/ScholarshipAll';
import LandingPage from './LandingPage';
require('dotenv').config();

function App() {
  return (
    <Router>
      <NavBar></NavBar>
      <Switch>
        <Route path="/" exact component={LandingPage}></Route>
        <Route path="/user" exact component={UserTab}></Route>

        <Route path="/athlete/:id/setting" exact component={Setting}></Route>
        <Route path="/club/:id/setting" exact component={Setting}></Route>
        
        <Route path="/athletes" exact component={AthleteAll}></Route>
        <Route path="/athlete/form" exact component={AthleteForm}></Route>
        <Route path="/athlete/:id" exact component={AthleteDetail}></Route>

        <Route path="/clubs" exact component={ClubAll}></Route>
        <Route path="/club/form" exact component={ClubForm}></Route>
        <Route path="/club/:id" exact component={ClubDetail}></Route>

        <Route path="/scholarship/form" exact component={ScholarshipForm}></Route>
        <Route path="/scholarships" exact component={ScholarshipAll}></Route>
        <Route path="/scholarship/:id" exact component={ScholarshipDetail}></Route>
      </Switch>
      <Footer></Footer>
    </Router>
  )
}

export default App;
