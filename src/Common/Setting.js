import React, { useState, useEffect } from 'react';
import { 
  Form, 
  Label, 
  Input, 
  FormGroup, 
  Container, 
  Col, 
  Alert,
  UncontrolledAlert, 
  Modal, 
  ModalHeader, 
  ModalBody, 
  ModalFooter, 
  Button } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import queryString from 'query-string';
import '../Pages/Athlete/AthleteForm/index.css';

const Setting = props => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [entity, setEntity] = useState('');
  const [submited, setSubmited] = useState(false);
  const [modal, setModal] = useState(false);
  const [checkEmail, setCheckEmail] = useState(false);
  const [isWrong, setIsWrong] = useState(false);
  const [isAppear, setIsAppear] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const parsed = queryString.parse(props.location.search);
  const backUrl = `${parsed.previousPage}`;

  const toggle = () => setModal(!modal);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/user/${sessionStorage.getItem('userId')}`, {
      method: 'GET'
    })
    .then(response => response.json())
    .then(result => {
      setEmail(result.data.email);
      setPassword(result.data.password);

      if (sessionStorage.getItem('registeredAs') === 'athlete') {
        setEntity('athlete');
      } else {
        setEntity('club');
      }
    })
    .then(err => err);
  }, []);

  let updatePlaceholder;
  if (entity === 'athlete') {
    updatePlaceholder = "Update data atlet";
  } else {
    updatePlaceholder = "Update data club";
  }

  function handleEmailChange(event) {
    if (email !== event.target.value) setEmail(event.target.value);
  }

  function handlePasswordChange(event) {
    setPassword(event.target.value);
  }

  function handleSubmitClick() {
    fetch(`${process.env.REACT_APP_BACKEND_API}/user/${sessionStorage.getItem('userId')}`, {
      method: 'PUT',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': sessionStorage.getItem('token')
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(response => response.json())
    .then(result => setSubmited(true))
    .catch(err => err);
  }

  function handleCheckEmailChange(event) {
    setCheckEmail(event.target.value);
  }

  function handleCheckEmailClick(event) {
    setIsAppear(true);
    if (email!== checkEmail) setIsWrong(true);
    else if (email === checkEmail) setIsWrong(false);
  }

  function handleDeleteClick(event) {
    fetch(`${process.env.REACT_APP_BACKEND_API}/athlete/${sessionStorage.getItem('id')}`, {
      method: 'DELETE',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': sessionStorage.getItem('token')
      }
    })
    .then(response => response.json())
    .then(result => {
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('userId');
      sessionStorage.removeItem('id');
      sessionStorage.removeItem('hasLoggedIn');
      sessionStorage.removeItem('justSignedUp');
      sessionStorage.removeItem('registeredAs');
      sessionStorage.removeItem('message');
      sessionStorage.removeItem('userMessage');
      sessionStorage.removeItem('athleteMessage');

      setIsDeleted(true);
      window.location.reload(true)
    })
    .catch(err => err);
  }

  let alert;
  if (submited === true) {
    alert = (
      <UncontrolledAlert>Update data has successfully been saved</UncontrolledAlert>
    );
  }

  let updateEntityUrl = `/${entity}/form`;

  let wrongAlert;
  let displayBtn = "d-none";
  if (isAppear) {
    if (isWrong === true) {
      wrongAlert = <Alert color="warning">Your password does not match</Alert>
      displayBtn = "d-none";
    } else if (isWrong === false) {
      wrongAlert = <Alert>Password match</Alert>;
      displayBtn = "d-block";
    }
  }

  if (isDeleted) {
    return <Redirect to="/"></Redirect>
  }

  return (
    <Container className="mt-3 mb-4 pl-md-4 pr-md-4">
      <p className="athlete-form-title">Setting</p>
      {alert}
      <Container>
        <p className="athlete-form-title">Update data account</p>
        <Form>
          <FormGroup className="d-flex athlete-form-group">
            <Col md="4">
              <Label for="userEmail">Email</Label>
            </Col>
            <Col md="8">
              <Input type="email" name="useremail" id="userEmail" onChange={handleEmailChange} 
                placeholder={email} value={email} required></Input>
            </Col>
          </FormGroup>
          <FormGroup className="d-flex athlete-form-group">
            <Col md="4">
              <Label for="userPassword">Password</Label>
            </Col>
            <Col md="8">
              <Input type="password" name="userpassword" id="userPassword" onChange={handlePasswordChange}></Input>
            </Col>
          </FormGroup>
          <div className="d-flex athlete-btn-container">
            <Button className="athlete-form-btn athlete-submit ml-3" onClick={handleSubmitClick}>Submit</Button>
          </div>
        </Form>
      </Container>
      <Container className="mt-3 mt-md-1 mb-4 pl-md-4 pr-md-4 pt-2 border-top">
        <p className="athlete-form-title">{updatePlaceholder}</p>
        <Link to={updateEntityUrl}>
          <Button className="ml-3">Update</Button>
        </Link>
      </Container>
      <Container className="mt-3 mb-4 pl-md-4 pr-md-4 pt-2 border-top">
        <p className="text-danger athlete-form-title">Delete account anda</p>
        <Button color="danger" onClick={toggle} className="ml-3">Delete</Button>
        <Modal isOpen={modal} toggle={toggle}>
          <ModalHeader toggle={toggle}>Apa anda yakin ingin menghapus akun anda?</ModalHeader>
          <ModalBody>
            <p>Akun yang telah terhapus tidak dapat digunakan kembali.</p>
            <Form>
              {wrongAlert}
              <Label for="checkPassword">Masukkan email anda</Label>
              <Input type="email" name="checkemail" id="checkEmail" onChange={handleCheckEmailChange}></Input>
              <Button type="button" color="primary" className="mt-2" onClick={handleCheckEmailClick}>Submit</Button>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="danger" className={displayBtn} onClick={handleDeleteClick}>Delete</Button>
            <Button color="secondary" onClick={toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </Container>
      <Container className="d-flex flex-row justify-content-end border-top pt-2">
        <Link to={backUrl}>
          <Button>Back</Button>
        </Link>
      </Container>
    </Container>
  )
}

export default Setting;