import React from 'react';
import { withRouter } from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import './Footer.css';

const Child = withRouter(props => {
	if (props.location.pathname !== '/user') {
		return (
			<div className="footer container-fluid d-flex">
				<Row className="align-items-center row-1">
					<Col xs="6" md="4" className="footer-atletnesia pl-md-4 pl-xl-5">
						<p>ATLETNESIA</p>
					</Col>
					<Col xs="6" md="8" className="footer-link-group">
						<ul className="col-md-6">
							<Link to="/athletes">
								<li className="text-white">Find Athlete</li>
							</Link>
							<Link to="/clubs">
								<li className="text-white">Find Club</li>
							</Link>
							<Link to="/scholarships">
								<li className="text-white">Find Scholarship</li>
							</Link>
						</ul>
						<ul className="col-md-6">
							<li>About Us</li>
							<li>Our Team</li>
							<li>Donation</li>
						</ul>
					</Col>
				</Row>
				<Row className="row-2 d-flex">
					<p>Contact Us</p>
					<ul className="d-flex flex-row footer-media-group">
						<li className="footer-media mr-3">
							<a href="https://facebook.com"><i className="fab fa-facebook-f"></i></a>
						</li>
						<li className="footer-media mr-3">
							<a href="https://twitter.com"><i className="fab fa-twitter"></i></a>
						</li>
						<li className="footer-media mr-3">
							<a href="https://instagram.com"><i className="fab fa-instagram"></i></a>
						</li>
						<li className="footer-media">
							<a href="https://linkedin.com"><i className="fab fa-linkedin-in"></i></a>
						</li>
					</ul>
				</Row>
			</div>
		)
	}
	else {
		return <div></div>
	}
});

export default Child;