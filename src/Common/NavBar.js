import React, { useState, useEffect } from 'react';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
	Button
} from 'reactstrap';
import { Link, useLocation } from 'react-router-dom';
import './NavBar.css';


const NavBar = (props) => {
	const [isOpen, setIsOpen] = useState(false);
	const [isLoggedIn, setIsLoggedIn] = useState(false);
	const [profPic, setProfPic] = useState('');
	const [entity, setEntity] = useState(null);
	let location = useLocation();

	const toggle = () => setIsOpen(!isOpen);

	useEffect(() => {
		if (sessionStorage.getItem('hasLoggedIn')) {
			setIsLoggedIn(true);

			if (sessionStorage.getItem('registeredAs') === 'athlete') setEntity('athlete');
			else if (sessionStorage.getItem('registeredAs') === 'club') setEntity('club');

			fetch(`${process.env.REACT_APP_BACKEND_API}/${sessionStorage.getItem('registeredAs')}/${sessionStorage.getItem('id')}`, {
				method: 'GET'
			})
			.then(response => response.json())
			.then(result => {
				if (result.data.profilePicture) setProfPic(result.data.profilePicture);
				else if (result.data.clubLogo) setProfPic(result.data.clubLogo);
			})
			.catch(err => err);
		}

	}, [profPic, entity]);
	
	let nav;
	const dashboardUrl = `/${entity}/${sessionStorage.getItem('id')}`;
	const settingUrl = `/${entity}/${sessionStorage.getItem('id')}/setting?previousPage=${location.pathname}`;

	// Navbar would be rendered as empty div if the current page is in login/sign up page
	if (location.pathname !== '/user') {
		if (isLoggedIn) {
			nav = (
				<Nav navbar>
					<UncontrolledDropdown nav inNavbar>
						<DropdownToggle nav caret>
							<img src={profPic} alt="athlete profile" className="navbar-profile"/>
						</DropdownToggle>
						<DropdownMenu right>
							<Link to={dashboardUrl}>
								<DropdownItem>Dashboard</DropdownItem>
							</Link>
							<DropdownItem divider></DropdownItem>
							<Link to={settingUrl}>
								<DropdownItem>Setting</DropdownItem>
							</Link>
							<DropdownItem divider></DropdownItem>
							<DropdownItem onClick={handleLogoutClick}>Logout</DropdownItem>
						</DropdownMenu>
					</UncontrolledDropdown>
				</Nav>
			)
		} else {
			nav = (
				<Nav navbar className="d-flex flex-row justify-content-center">
					<Link to="/user?tab=signIn">
						<Button className="bg-white text-primary ml-3 mr-1">Sign In</Button>
					</Link>
					<Link to="/user?tab=signUp">
						<Button className="bg-white text-secondary ml-1 mr-3">Sign Up</Button>
					</Link>
				</Nav>
			)
		}

		return (
			<div className="pr-md-5 pl-md-5 sticky-top bg-white">
				<Navbar expand="lg">
					<NavbarBrand href="/">ATLETNESIA</NavbarBrand>
					<NavbarToggler onClick={toggle}><i className="fas fa-bars"></i></NavbarToggler>
					<Collapse isOpen={isOpen} navbar>
						<Nav className="mr-auto" navbar>
							<NavItem>
								<NavLink>Discover</NavLink>
							</NavItem>
							<NavItem>
								<NavLink href="/clubs">Browse Club</NavLink>
							</NavItem>
							<NavItem>
								<NavLink href="/scholarships">Browse Scholarship</NavLink>
							</NavItem>
						</Nav>
						<Nav className="mt-1 mb-3 mt-md-0 mb-md-0 search-bar">
							<form className="pt-1 pb-1 d-flex flex-row justify-content-around">
								<input type="search" className="ml-1" placeholder="Cari Atlet"/>
								<button type="submit"><i className="fa fa-search"></i></button>
							</form>
						</Nav>

						{nav}

					</Collapse>
				</Navbar>
			</div>
		);
	} 
	else {
		return <div></div>
	}

	function handleLogoutClick(event) {
		event.preventDefault();

		sessionStorage.removeItem('token');
		sessionStorage.removeItem('userId');
		sessionStorage.removeItem('id');
		sessionStorage.removeItem('hasLoggedIn');
		sessionStorage.removeItem('justSignedUp');
		sessionStorage.removeItem('registeredAs');
		sessionStorage.removeItem('message');
		sessionStorage.removeItem('userMessage');
		sessionStorage.removeItem('athleteMessage');

		setIsLoggedIn(false);
		window.location.reload();
	}
}

export default NavBar;