import React, {useState} from 'react';
import {
  Button,
  Col,
  Row
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ReactComponent as Icon } from '../Asset/Artboard1.svg';
import Icon2 from '../Asset/Artboard2.svg'
import Icon3 from '../Asset/Artboard3.svg'
import Icon4 from '../Asset/Artboard4.svg'
import 'react-multi-carousel/lib/styles.css';
import Carousel from 'react-multi-carousel';

import './index.css'


const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 4,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 2,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1.5,
  },
};
const responsive2 = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 4,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1.2,
  },
};

const Home = (props) => {
  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed); //eslint-disable-line

 

 
    return (

      <div>
        
        <div>
          <Row>
            <Col>
          <section className="sect-1">
            <Icon className="icon-home" />
          </section>
          </Col>
          </Row>
          
         <Row>
            <Col>
              <Row>
                <div>
                  <h1 className="tulisan-kini">
                    Kini Saatnya Jadi Pahlawan Bangsa
            </h1>
                </div>
              </Row>
              <Row>
                <Col>
                <section className="sect-2">
                  <a href='/user?tab=sign-up'>
                  <Button className="daftar-sekarang" >DAFTAR SEKARANG</Button>
                  </a>
                </section>
                </Col>
              </Row>
            </Col>
            </Row>
         
        </div>
        <div className="home-div-2">
          <Row>
            <Col>
            <h3>
              Langkah mudah mewujudkan mimpimu menjadi Pahlawan Bangsa
            </h3>
            <p className="text-font-20">
              cukup ikuti 3 langkah mudah
              </p>
              </Col>
              </Row>
            <Row className="icon-kecil marginbot-100px">
              <Col md="4" xs="12">
                <img className="icon-2" src={Icon2} alt=""></img>
                <h4 className="margintop-50px">Pendaftaran</h4>
                <span>
                  Lengkapi syarat pendaftaran dengan menyiapkan CV dan video latihan terbaikmu
              </span>
              </Col>
              <Col md="4" xs="12">
                <img className="icon-3" src={Icon3} alt=""></img>
                <h4 className="margintop-50px">Menunggu Konfirmasi</h4>
                <span>
                  Tunggu kamu akan diseleksi oleh klub dan akan segera menghubungimu
              </span>
              </Col>
              <Col md="4" xs="12">
                <img className="icon-3" src={Icon4} alt=""></img>
                <h4 className="margintop-50px">Berhasil</h4>
                <span>
                  Selamat kamu telah berhasil gabung klub impianmu  jika persyaratannya memenuhi
              </span>
              </Col>
            </Row>
          
        </div>
        <div id="div-grey">
          <Row >
            <Col md="6" xs="12" >
              <div className="div-text-tersedia" >
              <h3>
                Beasiswa yang tersedia
            </h3>
              <span>
                terdapat 20 club yang bekerjasama dengan kami
            </span>
              <br></br>
              <br></br>
              <a href='/all-scholar'>
              <span>
                lihat lainnya
              </span>
              </a>
              </div>
            </Col>


            <Col md="6" xs="12" >
              <Carousel responsive={responsive}>
                <div className="box-1" >
                  <h5 style={{display:"inline-block", paddingTop:"15px", marginLeft:"20px"}}> Atlet Renang</h5>
                  <button className='btn-detail' style={{display:"inline-block"}}>Detail</button>
                  <hr />

                </div>
                <div className="box-1 "><h5 style={{display:"inline-block", paddingTop:"15px", marginLeft:"20px"}} > Atlet Renang</h5>
                <button className='btn-detail' style={{display:"inline-block"}}>Detail</button>
                  <hr />
                </div>
                <div className="box-1 "><h5 style={{display:"inline-block", paddingTop:"15px", marginLeft:"20px"}} > Atlet Renang</h5>
                <button className='btn-detail' style={{display:"inline-block"}}>Detail</button>
                  <hr />
                </div>
                <div className="box-1"><h5 style={{display:"inline-block", paddingTop:"15px", marginLeft:"20px"}}> Atlet Renang</h5>
                <button className='btn-detail' style={{display:"inline-block"}}>Detail</button>
                  <hr />
                </div>
              </Carousel>
            </Col>
          </Row>

        </div>
        <div id="div-hire">
          <h1 id="hire-calon">
            HIRE CALON ATLET
          </h1>
          <span className="text-white">
            Cari atlet-atlet berbakat dengan mendaaftarkan perusahaanmu disini
          </span>
          <section>
            <a href='/post-scholar/:id'>
            <Button className="btn-post-sch" >POST SCHOLARSHIP</Button>
            </a>
          </section>
        </div>
        <div id="div-katamereka">
          <h1>
            APA KATA MEREKA?
          </h1>
          <Carousel responsive={responsive2}>
            <div className="box-2 margin-LR-25px">
              <span className="bulet">
              </span>
              <p className="margintop-5px">
              </p>
              <span className="margintop-5px">
                Taufik
              </span>
              <p>
                Atlet dari klub A
              </p>
              <section className="margintop-5px margin-LR-35px">
                <span >
                  "Menjadi seorang atlet merupakan impianku sejak kecil. Beruntung menemukan platform yang bias menghubungkan ke klub favorit dengan biaya yang murah"
              </span>
              </section>
            </div>
            <div className="box-2  ">
              <span className="bulet">
              </span>
              <p className="margintop-5px">
              </p>
              <span className="margintop-5px">
                Taufik
              </span>
              <p>
                Atlet dari klub A
              </p>
              <section className="margintop-5px margin-LR-35px">
                <span >
                  "Menjadi seorang atlet merupakan impianku sejak kecil. Beruntung menemukan platform yang bias menghubungkan ke klub favorit dengan biaya yang murah"
              </span>
              </section>
            </div>
            <div className="box-2  ">
              <span className="bulet">
              </span>
              <p className="margintop-5px">
              </p>
              <span className="margintop-5px">
                Taufik
              </span>
              <p>
                Atlet dari klub A
              </p>
              <section className="margintop-5px margin-LR-35px">
                <span >
                  "Menjadi seorang atlet merupakan impianku sejak kecil. Beruntung menemukan platform yang bias menghubungkan ke klub favorit dengan biaya yang murah"
              </span>
              </section>
            </div>
            <div className="box-2  ">
              <span className="bulet">
              </span>
              <p className="margintop-5px">
              </p>
              <span className="margintop-5px">
                Taufik
              </span>
              <p>
                Atlet dari klub A
              </p>
              <section className="margintop-5px margin-LR-35px">
                <span >
                  "Menjadi seorang atlet merupakan impianku sejak kecil. Beruntung menemukan platform yang bias menghubungkan ke klub favorit dengan biaya yang murah"
              </span>
              </section>
            </div>
          </Carousel>

        </div>

      </div>
    );

  
}

export default Home;

