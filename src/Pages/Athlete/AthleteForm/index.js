import React, { useState, useEffect } from 'react';
import {
	Container,
	Col,
	Button,
	Form,
	FormGroup,
	Label,
	Input,
	FormText
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import queryString from 'query-string';
import './index.css';

const AthleteForm = (props) => {
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [sports, setSports] = useState([]);
	const [athleteSport, setAthleteSport] = useState('');
	const [athleteSportId, setAthleteSportId] = useState('');
	const [bornDate, setBornDate] = useState('');
	const [location, setLocation] = useState('');
	const [desc, setDesc] = useState('');
	const [profilePic, setProfilePic] = useState('');
	const [currVit, setCurrVit] = useState(''); //eslint-disable-line
	const [video, setVideo] = useState('');
	const [submited, setSubmited] = useState(false);
	const parsed = queryString.parse(props.location.search);
	let button;

	console.log(athleteSportId)

	useEffect(() => {
		Promise.all([
			fetch(`${process.env.REACT_APP_BACKEND_API}/user/${sessionStorage.getItem('userId')}`, {method: 'GET'}),
			fetch(`${process.env.REACT_APP_BACKEND_API}/athlete/${sessionStorage.getItem('id')}`, {method: 'GET'}),
			fetch(`${process.env.REACT_APP_BACKEND_API}/sports`, {method: 'GET'})
		])
		.then(allResponses => {
			return [allResponses[0].json(), allResponses[1].json(), allResponses[2].json()];
		})
		.then(results => {
			const userResult = results[0];
			const athleteResult = results[1];
			const sportsResult = results[2];

			userResult.then(data => {
				setFirstName(data.data.firstName);
				setLastName(data.data.lastName);
			})
			.catch(err => err);

			athleteResult.then(data => {
				setBornDate(data.data.bornDate);
				setLocation(data.data.location);
				setProfilePic(data.data.profilePicture);
				setCurrVit(data.data.curriculumVitae);
				setVideo(data.data.video);
				setDesc(data.data.about);
				setAthleteSportId(data.data.sportId._id);
				setAthleteSport(data.data.sportId.sportName);
			})
			.catch(err => err);

			sportsResult.then(data => {
				// get all documents and its properties
				//setSports(data.data)
				setTimeout(() => setSports(data.data), 1000);
			})
		})
		.catch(err => err);

	}, []);

	function handleFirstNameChange(event) {
		if (firstName !== event.target.value) setFirstName(event.target.value);
	}

	function handleLastNameChange(event) {
		if (lastName !== event.target.value) setLastName(event.target.value);
	}

	function handleSportChange(event) {
		if (athleteSport !== event.target.value) setAthleteSportId(event.target.value);
	}

	function handleDateChange(event) {
		if (bornDate !== event.target.value) setBornDate(event.target.value);
	}

	function handleLocationChange(event) {
		if (location !== event.target.value) setLocation(event.target.value);
	}

	function handleDescChange(event) {
		if (desc !== event.target.value) setDesc(event.target.value);
	}

	function handleImageChange(event) {
		const formData = new FormData();

		formData.append('file', event.target.files[0]);
		formData.append('upload_preset', process.env.REACT_APP_CLOUDINARY_UPLOAD_PRESET);

		fetch(process.env.REACT_APP_CLOUDINARY_UPLOAD_URL, {
			method: 'POST',
			body: formData
		})
		.then(response => response.json())
		.then(data => {
			setProfilePic(data.secure_url);
		})
		.catch(err => err);
	}

	function handleCurrChange(event) {
		// still trying to figure out...
	}

	function handleVideoChange(event) {
		const formData = new FormData();

		formData.append('file', event.target.files[0]);
		formData.append('upload_preset', process.env.REACT_APP_CLOUDINARY_UPLOAD_PRESET);

		fetch(process.env.REACT_APP_CLOUDINARY_UPLOAD_URL, {
			method: 'POST',
			body: formData
		})
		.then(response => response.json())
		.then(data => setVideo(data.secure_url))
		.catch(err => err);
	}

	function handleSubmitClick(event) {
		Promise.all([
			// this will update to /athlete/:id
			fetch(`${process.env.REACT_APP_BACKEND_API}/athlete/${sessionStorage.getItem('id')}`, {
				method: 'PUT',
				cache: 'no-cache',
				credentials: 'same-origin',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': sessionStorage.getItem('token')
				},
				body: JSON.stringify({
					bornDate: bornDate,
					location: location,
					sportId: athleteSportId,
					profilePicture: profilePic,
					curriculumVitae: currVit,
					video: video,
					about: desc
				})
			}),
			// this will update to /user/:id
			fetch(`${process.env.REACT_APP_BACKEND_API}/user/${sessionStorage.getItem('userId')}`, {
				method: 'PUT',
				cache: 'no-cache',
				credentials: 'same-origin',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': sessionStorage.getItem('token')
				},
				body: JSON.stringify({
					firstname: firstName,
					lastName: lastName
				})
			})
		])
		.then(allResponses => {
			return [allResponses[0].json(), allResponses[1].json()];
		})
		.then(results => {
			const athleteResult = results[0];
			const userResult = results[1];

			athleteResult.then(data => {
				sessionStorage.setItem('athleteMessage', data.message);
				return data
			})
			.catch(err => err);

			userResult.then(data => {
				sessionStorage.setItem('userMessage', data.message);
				return data;
			})
			.catch(err => err);

			setSubmited(true);
			// this is used because profile pic in navbar wouldn't be updated unless the page is reloaded.
			window.location.reload(true);
		})
		.catch(err => err);
	}

	let sportVal;
	let sportPlaceholder;
	
	if (athleteSport === '') {
		sportVal = '';
		sportPlaceholder = '--Pilih salah satu--';
	}
	else {
		sportVal = athleteSportId;
		sportPlaceholder = athleteSport;
	}

	if (parsed.justSignedUp === 'true') {
		button = <Button className="athlete-form-btn back-btn" hidden>Back</Button>
	} else {
		button = <Button className="athlete-form-btn back-btn">Back</Button>
	}

	let url = `/athlete/${sessionStorage.getItem('id')}`;
	if (submited) {
		return <Redirect to={url}></Redirect>
	}

	return (
		<Container className="mt-3 mb-4 pl-md-4 pr-md-4">
			<p className="athlete-form-title">Profil Atlet</p>
			<Form encType="multipart/form-data">
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="firstName">Nama Depan</Label>
					</Col>
					<Col md="8">
						<Input type="text" name="firstname" id="firstName" onChange={handleFirstNameChange} 
							placeholder={firstName} value={firstName}></Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="lastName">Nama Belakang</Label>
					</Col>
					<Col md="8">
						<Input type="text" name="lastname" id="lastName" onChange={handleLastNameChange} 
							placeholder={lastName} value={lastName}></Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group athlete-form-date">
					<div className="d-flex athlete-date-input">
						<Col md="4">
							<Label for="bornDate">Tanggal Lahir</Label>
						</Col>
						<Col md="8">
							<Input type="date" name="borndate" id="bornDate" onChange={handleDateChange}></Input>
						</Col>
					</div>
					<FormText className="date-format">
						format tanggal (mm/dd/yyyy)
					</FormText>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="athleteSport">Cabang Olahraga</Label>
					</Col>
					<Col md="8">
						<Input type="select" name="athletesport" id="athleteSport" onChange={handleSportChange}>
							<option value={sportVal}>{sportPlaceholder}</option>
							{sports.map(sport => {
								return <option key={sport._id} value={sport._id}>{sport.sportName}</option>
							})}
						</Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="location">Alamat</Label>
					</Col>
					<Col md="8">
						<Input type="textarea" name="location" id="location" 
							onChange={handleLocationChange} placeholder={location} value={location}></Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="selfDescription">Deskripsi Diri</Label>
					</Col>
					<Col md="8">
						<Input type="textarea" name="selfdescription" id="selfDescription" 
							onChange={handleDescChange} placeholder={desc} value={desc}></Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="profilePicture">Foto Profil</Label>
					</Col>
					<Col md="8">
						<Input type="file" name="profilepicture" id="profilePicture" onChange={handleImageChange}></Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="curriculumVitae">Curriculum Vitae</Label>
					</Col>
					<Col md="8">
						<Input type="file" name="curriculumvitae" id="curriculumVitae" onChange={handleCurrChange}></Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="video">Video</Label>
					</Col>
					<Col md="8">
						<Input type="file" name="video" id="video" onChange={handleVideoChange}></Input>
					</Col>
				</FormGroup>
				<div className="d-flex athlete-btn-container">
					<Link to={url}>{button}</Link>
					<Button className="athlete-form-btn athlete-submit ml-3" onClick={handleSubmitClick}>Submit</Button>
				</div>
			</Form>
		</Container>
	);
}

export default AthleteForm;