import React, { useState, useEffect } from 'react';
import { 
	Row,
	Card,
	CardHeader,
	CardBody,
	Col,
	Button,
	Alert
} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import '../AthleteDetail/ClubApplied.css';
import '../../Scholarship/ScholarshipAll/index.css';


const AthleteAll = props => {
	const [athletes, setAthletes] = useState([]);
	const [athleteId, setAthleteId] = useState('');

	useEffect(() => {
		fetch(`${process.env.REACT_APP_BACKEND_API}/athletes`, {
			method: 'GET'
		})
		.then(response => response.json())
		.then(result => {
			setAthletes([...athletes, ...result.data]);
		})
		.catch(err => err);
	}, []); //eslint-disable-line

	const redirectFunc = id => setAthleteId(id);

	if (athleteId) {
		const url = `/athlete/${athleteId}`;
		return <Redirect to={url}></Redirect>
	} 

	let alert;
	if (athletes.length === 0) {
		alert = <Alert className="text-center" color="warning">There are no athletes available right now :(.</Alert>
	}

	return (


		<div className="container-fluid width-75 ml-auto mr-auto mb-4">
			<Row className="club-row-1 pt-4 pl-4 pl-md-5 pl-xl-0 ml-xl-2">
				<p>Daftar Atlet</p>
			</Row>
			<Row className="justify-content-center">
				{alert}

				{athletes.map(athlete => {
					return (
						<Card className="ml-md-3 mr-md-3 mt-3 mb-3" key={athlete._id}>
							<CardHeader className="d-flex flex-row club-card-header">
								<Col xs="7" className="club-header-text">
									<p className="club-type">{athlete.status} {athlete.sportId !== undefined ? athlete.sportId.sportName : ''}</p>
								</Col>
								<Col xs="5" className="club-header-btn">
									<Button onClick={() => redirectFunc(athlete._id)}>Detail</Button>
								</Col>
							</CardHeader>
							<CardBody className="d-flex flex-row club-card-body">
								<img src={athlete.profilePicture} 
									alt="club logo" className="club-media"/>
								<div className="d-flex flex-column club-card-body-text">
									<p>{athlete.userId.firstName} {athlete.userId.lastName}</p>
									<p>{athlete.location}</p>
								</div>
							</CardBody>
						</Card>
					)
				})}
			</Row>
		</div>
	)
}

export default AthleteAll;