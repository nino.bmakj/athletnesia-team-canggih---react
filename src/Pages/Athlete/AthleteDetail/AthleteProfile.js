import React from 'react';
import { Row, Col, Alert, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import './AthleteProfile.css';

export default class AthleteProfile extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false
		}
		this.handleAboutClick = this.handleAboutClick.bind(this);
		this.onDismiss = this.onDismiss.bind(this);
	}
	
	handleAboutClick() {
		this.setState({visible: !this.state.visible});
	}

	onDismiss() {
		this.setState({visible: false});
	}

	render() {
		const first = `${this.props.athlete.firstName.charAt(0).toUpperCase() + this.props.athlete.firstName.slice(1)}`;
		const last = ` ${this.props.athlete.lastName.charAt(0).toUpperCase() + this.props.athlete.lastName.slice(1)}`;
		const athleteName = `${first} ${last}`;
		const athleteStatus = `${this.props.athlete.status} ${this.props.athlete.sport}`;
		const athleteAge = `${this.props.athlete.age} tahun`;
		const athleteLocation = `${this.props.athlete.location}`;

		// Below code needs to be modified in order to not allow just any login user to have access to update button
		let updateBtn;
		if (sessionStorage.getItem('hasLoggedIn') && sessionStorage.getItem('id') === this.props.id) {
			updateBtn = (
				<div className="d-flex flex-row justify-content-end mr-auto ml-auto pb-3 pr-md-5 width-xl">
					<Link to="/athlete/form">
						<Button color="primary">Update Profile</Button>
					</Link>
				</div>
			)
		}

		return (
			<div className="container-fluid top-container">
				<Row className="pt-4 pl-md-5 pr-md-5 mr-xl-auto ml-xl-auto width-xl">
					<Col xs="6" md="3" lg="2">
						<img src={this.props.athlete.profilePic} alt="athlete profile" className="athlete-media"/>
					</Col>
					<Col xs="6" md="8" lg="9" className="athlete-detail-group">
						<p className="athlete-name m-0 mb-3">{athleteName}</p>
						<p className="athlete-detail m-0">{athleteStatus}</p>
						<p className="athlete-detail m-0">{athleteAge}</p>
						<p className="athlete-detail m-0">{athleteLocation}</p>
					</Col>
				</Row>
				<Row className="pt-3 athlete-link-container pl-md-5 pr-md-5 mr-xl-auto ml-xl-auto width-xl">
					<ul className="d-flex flex-row ml-md-2 mr-md-2 athlete-link-group">
						<li className="athlete-link mr-3"><a href={this.props.athlete.cv}>CV</a></li>
						<li className="athlete-link mr-3"><a href={this.props.athlete.letter}>Letter</a></li>
						<li className="athlete-link mr-3"><a href={this.props.athlete.video}>Video</a></li>
						{/* eslint-disable-next-line*/}
						<li className="athlete-link"><a href="#" onClick={this.handleAboutClick}>About</a></li>
					</ul>
				</Row>
				<Alert color="info" className="w-75 ml-auto mr-auto" isOpen={this.state.visible} toggle={this.onDismiss}>
					{this.props.athlete.about}
				</Alert>
				{updateBtn}
			</div>
		);
	}

}