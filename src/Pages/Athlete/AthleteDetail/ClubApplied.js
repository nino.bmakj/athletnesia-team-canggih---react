import React from 'react';
import { Row, Col, Card, CardHeader, CardBody, Button, UncontrolledAlert, Alert } from 'reactstrap';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { Link } from 'react-router-dom';
import './ClubApplied.css';


export default class ClubApplied extends React.Component {
	render() {
		const responsive = {
			xl: {
				breakpoint: {max: 3000, min: 1200},
				items: 3,
			},
			lg: {
				breakpoint: {max: 1199, min: 992},
				items: 3
			},
			md: {
				breakpoint: {max: 991, min: 768},
				items: 2
			},
			sm: {
				breakpoint: {max: 767, min: 576},
				items: 2
			},
			xs: {
				breakpoint: {max: 576, min: 0},
				items: 1
			}
		}

		/* 
		this is used in situation where there is only one card that would be rendered.
		If responsive2 is not defined, the card would not be appeared.
		*/
		/*
		const responsive2 = {
			xl: {
				breakpoint: {max: 3000, min: 1200},
				items: 1,
				width: '30vw'
			},
			lg: {
				breakpoint: {max: 1199, min: 992},
				items: 1,
				width: '40vw'
			},
			md: {
				breakpoint: {max: 991, min: 768},
				items: 1,
				width: '60vw'
			},
			sm: {
				breakpoint: {max: 767, min: 576},
				items: 1
			},
			xs: {
				breakpoint: {max: 576, min: 0},
				items: 1
			}	
		}

		let responsiveType;
		let alert;
		if (this.state.appliedClub.length > 1) {
			responsiveType = responsive;
		} 
		else if (this.state.appliedClub.length === 1) {
			responsiveType = responsive2;
		} 
		else {
			responsiveType = responsive2;
			alert = (
				<div className="d-flex flex-column justify-content-center align-items-center mt-3 mt-md-5">
					<UncontrolledAlert color="warning">
						You are not applying to any club, yet
					</UncontrolledAlert>
					<Alert className="text-center" color="info">
						<p>Want to start applying to a club now?</p>
						<Link to="/clubs">
							<Button color="primary">Find a clubs</Button>
						</Link>
					</Alert>
				</div>
			)
		}
		*/
		let alert;
		if (this.props.appliedClub.length === 0) {
			alert = (
				<div className="d-flex flex-column justify-content-center align-items-center mt-3 mt-md-5">
					<UncontrolledAlert color="warning">
						You are not applying to any club, yet
					</UncontrolledAlert>
					<Alert className="text-center" color="info">
						<p>Want to start applying to a club now?</p>
						<Link to="/clubs">
							<Button color="primary">Find a clubs</Button>
						</Link>
					</Alert>
				</div>
			)
		}

		return (
			<div className="container-fluid club-container">
				<Row className="club-row-1 pl-4 pl-md-3">
					<p className="ml-md-4">Club Applied</p>
				</Row>
				<div className="club-row-2">
					{alert}
					<Carousel
					  responsive={responsive}
					  swipeable={false}
					  draggable={false}
					  showDots={true}
					  infinite={true}
					  keyBoardControl={true}
					  transitionDuration={500}
					  dotListClass="custom-dot-list-style"
					  containerClass="carousel-container"
					>
						{this.props.appliedClub.map(club => {
							const url = `/club/${club._id}`;

							return (
								<Card className="ml-md-3 mr-md-3" key={club._id}>
									<CardHeader className="d-flex flex-row club-card-header">
										<Col xs="8" className="club-header-text">
											<p className="club-type">Cabang {club.sportId.sportName}</p>
											<small className="club-status">waiting confirmation</small>
										</Col>
										<Col xs="4" className="club-header-btn">
											<Link to={url}>
												<Button>Detail</Button>
											</Link>
										</Col>
									</CardHeader>
									<CardBody className="d-flex flex-row club-card-body">
										<img src={club.clubLogo}
											alt="club logo" className="club-media"/>
										<div className="d-flex flex-column club-card-body-text">
											<p>{club.userId.firstName}</p>
											<p>{club.clubAddress}</p>
										</div>
									</CardBody>
								</Card>
							)
						})}

					</Carousel>
				</div>
			</div>
		)
	}
}