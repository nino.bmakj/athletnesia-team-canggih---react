import React from 'react';
import { Row, Col, Card, CardHeader, CardBody, Button, Alert } from 'reactstrap';
import { Link } from 'react-router-dom';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import './ClubApplied.css';
import './ClubRecommend.css';

export default class ClubRecommend extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			clubs: []
		}
	}

	componentDidMount() {
		fetch(`${process.env.REACT_APP_BACKEND_API}/clubs`, {method: 'GET'})
			.then(response => response.json())
			.then(result => {
				this.setState({
					clubs: [...this.state.clubs, ...result.data]
				});
			})
	}
	
	render() {
		const responsive = {
			xl: {
				breakpoint: {max: 3000, min: 1200},
				items: 3,
				partialVisibilityGutter: 15
			},
			lg: {
				breakpoint: {max: 1199, min: 992},
				items: 2,
				partialVisibilityGutter: 100
			},
			md: {
				breakpoint: {max: 991, min: 768},
				items: 2,
				partialVisibilityGutter: 35
			},
			sm: {
				breakpoint: {max: 767, min: 576},
				items: 2
			},
			xs: {
				breakpoint: {max: 576, min: 0},
				items: 1
			}
		}
		/*
		const responsive2 = {
			xl: {
				breakpoint: {max: 3000, min: 1200},
				items: 1
			},
			lg: {
				breakpoint: {max: 1199, min: 992},
				items: 1
			},
			md: {
				breakpoint: {max: 991, min: 768},
				items: 1
			},
			sm: {
				breakpoint: {max: 767, min: 576},
				items: 1
			},
			xs: {
				breakpoint: {max: 576, min: 0},
				items: 1
			}	
		}

		let responsiveType;
		let alert;
		if (this.state.clubs.length > 1) {
			responsiveType = responsive;
		} 
		else if (this.state.clubs.length === 1) {
			responsiveType = responsive2;
		} else {
			responsiveType = responsive2;
			alert = (
				<div className="d-flex flex-column justify-content-center align-items-center mt-3 mb-3 mb-md-5 mt-md-5">
					<Alert className="text-center" color="warning">There are no clubs in associated sport right now.</Alert>
					<Alert className="text-center" color="info">
						<p>Want to start applying a scholarship now?</p>
						<Link to="/scholarships">
							<Button color="primary">Find a scholarship</Button>
						</Link>
					</Alert>
				</div>
			)
		}
		*/
		let alert;
		if (this.state.clubs.length === 0) {
			alert = (
				<div className="d-flex flex-column justify-content-center align-items-center mt-3 mb-3 mb-md-5 mt-md-5">
					<Alert className="text-center" color="warning">There are no clubs in associated sport right now.</Alert>
					<Alert className="text-center" color="info">
						<p>Want to start applying a scholarship now?</p>
						<Link to="/scholarships">
							<Button color="primary">Find a scholarship</Button>
						</Link>
					</Alert>
				</div>
			)
		}

		const titleStr = this.props.titleStr !== undefined ? this.props.titleStr : "RECOMMENDED clubs for you";

		return (
			<div className="container-fluid recommend-container">
				<Row className="club-row-1 d-flex row-1-container">
					<p className="ml-md-4">{titleStr}</p>
					<form id="search-form" className=" d-flex flex-row-reverse justify-content-between mr-md-4">
						<input type="text" placeholder="Cari Beasiswa"/>
						<button type="submit"><i className="fas fa-search"></i></button>
					</form>
				</Row>
				<div className="club-row-2">
					<Carousel
						partialVisible={true}
					  responsive={responsive}
					  swipeable={false}
					  draggable={false}
					  showDots={true}
					  infinite={true}
					  keyBoardControl={true}
					  transitionDuration={500}
					>
						{this.state.clubs.map(club => {
							const url = `/club/${club._id}`

							return (
								<Card className="ml-md-3 mr-md-3" key={club._id}>
									<CardHeader className="d-flex flex-row club-card-header">
										<Col xs="8" className="club-header-text">
											<p className="club-type">Cabang {club.sportId ? club.sportId.sportName : '' }</p>
										</Col>
										<Col xs="4" className="club-header-btn">
											<Link to={url}>
												<Button>Detail</Button>
											</Link>
										</Col>
									</CardHeader>
									<CardBody className="d-flex flex-row club-card-body">
										<img src={club.clubLogo} 
											alt="club logo" className="club-media"/>
										<div className="d-flex flex-column club-card-body-text">
											<p>{club.userId.firstName}</p>
											<p>{club.location}</p>
										</div>
									</CardBody>
								</Card>
							)
						})}
					</Carousel>

					{alert}
					
				</div>
			</div>
		);	
	}
}