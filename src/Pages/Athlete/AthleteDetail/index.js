import React from 'react';
import AthleteProfile from './AthleteProfile';
import ClubApplied from './ClubApplied';
import ClubRecommend from './ClubRecommend';

export default class Athlete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      status: '',
      sport: '',
      age: '',
      location: '',
      cv: null,
      letter: null,
      video: null,
      about: 'There is no description, yet.',
      profilePic: '',
      appliedClub: []
    }
  }

  componentDidMount() {
    fetch(`${process.env.REACT_APP_BACKEND_API}/athlete/${this.props.match.params.id}`, {
      method: 'GET'
    })
    .then(response => response.json())
    .then(result => {
      // calculating age
      const today = new Date();
      const born = new Date(result.data.bornDate);

      let diffYear = today.getFullYear() - born.getFullYear();
      const diffMonth = today.getMonth() - born.getMonth();
      const diffDate = today.getDate() - born.getDate();

      if (diffMonth < 0) diffYear -= 1; // if current month hasn't passed born month
      if (diffMonth === 0 && diffDate < 0) diffYear -= 1; // if it's already in born month but hasn't passed the born date

      // change state
      setTimeout(() => {
        this.setState({
          firstName: result.data.userId.firstName,
          lastName: result.data.userId.lastName,
          status: result.data.status,
          age: diffYear,
          sport: result.data.sportId ? result.data.sportId.sportName : '',
          location: result.data.location,
          profilePic: result.data.profilePicture,
          cv: result.data.curriculumVitae,
          video: result.data.video,
          about: result.data.about,
          appliedClub: [...this.state.appliedClub, ...result.data.appliedClub]
        });
      }, 500)
    })
    .catch(err => err);
  }

	render() {
		return (
			<div>
				<AthleteProfile id={this.props.match.params.id} athlete={this.state}></AthleteProfile>
				<ClubApplied id={this.props.match.params.id} appliedClub={this.state.appliedClub}></ClubApplied>
				<ClubRecommend clubs={this.state.appliedClub}></ClubRecommend>
			</div>
		)
	}
}