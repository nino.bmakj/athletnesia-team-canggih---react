import React, { useState, useEffect } from 'react';
import { 
	Row,
	Card,
	CardHeader,
	CardBody,
	Col,
	Button,
	Alert
} from 'reactstrap';
import { Link } from 'react-router-dom';
import '../../Athlete/AthleteDetail/ClubApplied.css';
import '../../Scholarship/ScholarshipAll/index.css';


const ClubAll = props => {
	const [clubs, setClubs] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_BACKEND_API}/clubs`, {
			method: 'GET'
		})
		.then(response => response.json())
		.then(result => {
			setClubs([...clubs, ...result.data]);
		})
		.catch(err => err);
	}, []); //eslint-disable-line

	let alert;
	if (clubs.length === 0) {
		alert = <Alert className="text-center" color="warning">There are no clubs available right now :(.</Alert>
	}

	return (
		<div className="container-fluid width-75 ml-auto mr-auto mb-4 h-100">
			<Row className="club-row-1 pt-4 pl-4 pl-md-5 pl-xl-0 ml-xl-2">
				<p>Daftar Club</p>
			</Row>
			{alert}
			<Row className="justify-content-center">
				{clubs.map(club => {
					const url = `/club/${club._id}`;

					return (
						<Card className="ml-md-3 mr-md-3 mt-3 mb-3" key={club._id}>
							<CardHeader className="d-flex flex-row club-card-header">
								<Col xs="7" className="club-header-text">
									<p className="club-type">Klub {club.sportId ? club.sportId.sportName : '' }</p>
								</Col>
								<Col xs="5" className="club-header-btn">
									<Link to={url}>
										<Button>Detail</Button>
									</Link>
								</Col>
							</CardHeader>
							<CardBody className="d-flex flex-row club-card-body">
								<img src={club.clubLogo} 
									alt="club logo" className="club-media"/>
								<div className="d-flex flex-column club-card-body-text">
									<p>{club.userId.firstName}</p>
									<p>{club.clubAddress}</p>
								</div>
							</CardBody>
						</Card>
					)
				})}
			</Row>
		</div>
	)
}

export default ClubAll;