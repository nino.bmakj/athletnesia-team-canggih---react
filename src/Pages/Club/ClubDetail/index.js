import React, { useState, useEffect } from 'react';
import ClubProfile from './ClubProfile';
import ScholarshipPost from './ScholarshipPost';
import AthleteRecommend from './AthleteRecommend';
import ClubRecommend from '../../Athlete/AthleteDetail/ClubRecommend';

const ClubDetail = props => {
  const [clubId, setClubId] = useState('');
  const [clubLogo, setClubLogo] = useState('');
  const [clubName, setClubName] = useState('');
  const [clubAbout, setClubAbout] = useState('');
  const [clubAddress, setClubAddress] = useState('');
  const [clubPhone, setClubPhone] = useState('');
  const [scholarships, setScholarships] = useState([]);
  const club = {clubLogo, clubName, clubAbout, clubAddress, clubPhone}

  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_API}/club/${props.match.params.id}`, {
      method: 'GET'
    })
    .then(response => response.json())
    .then(result => {
      setClubId(result.data._id);
      setClubLogo(result.data.clubLogo);
      setClubName(result.data.userId.firstName);
      setClubAddress(result.data.clubAddress);
      setClubPhone(result.data.clubPhone);
      setClubAbout(result.data.clubAbout);
      setScholarships([...scholarships, ...result.data.scholarshipOffered]);
    })
  }, []) //eslint-disable-line

  let recommend;
  if (sessionStorage.getItem('hasLoggedIn')) {
    const titleStr = sessionStorage.getItem('clubId') === clubId ? "Recommended athletes for you" : "You may also want to know these athletes";
    if (sessionStorage.getItem('registeredAs') === 'club') {
      recommend = <AthleteRecommend titleStr={titleStr}></AthleteRecommend>
    } else if (sessionStorage.getItem('registeredAs' === 'athlete')) {
      recommend = <ClubRecommend titleStr={"You may also want to know these clubs"}></ClubRecommend>
    }
  }
  else {
    recommend = (
      <div>
        <AthleteRecommend titleStr={"You may also want to know these athletes"}></AthleteRecommend>
        <ClubRecommend titleStr={"You may also want to know these clubs"}></ClubRecommend>
      </div>
    )
  }

	return (
		<div>
			<ClubProfile id={props.match.params.id} club={club}></ClubProfile>
			<ScholarshipPost id={props.match.params.id} scholarships={scholarships}></ScholarshipPost>
			{recommend}
		</div>
	)
}

export default ClubDetail;