import React from 'react';
import { 
	Row,
	Col,
	Card,
	CardHeader,
	CardBody,
	Button,
	Progress,
	Alert,
	UncontrolledAlert
} from 'reactstrap';
import { Link } from 'react-router-dom';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import '../../Athlete/AthleteDetail/ClubApplied.css';
import './ScholarshipPost.css';


export default class ScholarshipPost extends React.Component {
	render() {
		const responsive1 = {
			xl: {
				breakpoint: {max: 3000, min: 1200},
				items: 3,
				partialVisibilityGutter: 15
			},
			lg: {
				breakpoint: {max: 1199, min: 992},
				items: 2,
				partialVisibilityGutter: 100
			},
			md: {
				breakpoint: {max: 991, min: 768},
				items: 2,
				partialVisibilityGutter: 35
			},
			sm: {
				breakpoint: {max: 767, min: 576},
				items: 2
			},
			xs: {
				breakpoint: {max: 576, min: 0},
				items: 1
			}
		}
		/*
		const responsive2 = {
			xl: {
				breakpoint: {max: 3000, min: 1200},
				items: 1
			},
			lg: {
				breakpoint: {max: 1199, min: 992},
				items: 1
			},
			md: {
				breakpoint: {max: 991, min: 768},
				items: 1
			},
			sm: {
				breakpoint: {max: 767, min: 576},
				items: 1
			},
			xs: {
				breakpoint: {max: 576, min: 0},
				items: 1
			}	
		}

		let responsiveType;
		let alert;
		if (this.state.scholarships.length > 1) {
			responsiveType = responsive1;
		} 
		else if (this.state.scholarships.length === 1) {
			responsiveType = responsive2;
		} 
		else {
			responsiveType = responsive2;
			alert = (
				<div className="d-flex flex-column justify-content-center align-items-center mt-3 mt-md-5">
					<UncontrolledAlert color="warning">
						You are not posting any scholarships, yet
					</UncontrolledAlert>
					<Alert className="text-center" color="info">
						<p>Want to start posting one now?</p>
						<Link to="/scholarship/form">
							<Button color="primary">Post a scholarship</Button>
						</Link>
					</Alert>
				</div>
			)
		}
		*/
		let alert;
		if (this.props.scholarships.length === 0) {
			alert = (
				<div className="d-flex flex-column justify-content-center align-items-center mt-3 mt-md-5">
					<UncontrolledAlert color="warning">
						You are not posting any scholarships, yet
					</UncontrolledAlert>
					<Alert className="text-center" color="info">
						<p>Want to start posting one now?</p>
						<Link to="/scholarship/form">
							<Button color="primary">Post a scholarship</Button>
						</Link>
					</Alert>
				</div>
			)
		}

		return (
			<div className="container-fluid club-container">
				<Row className="club-row-1 pl-4 pl-md-3">
					<p className="ml-md-4">Scholarship Posted</p>
				</Row>
				<div className="club-row-2">
					{alert}
					<Carousel
						partialVisible={true}
					  responsive={responsive1}
					  swipeable={false}
					  draggable={false}
					  infinite={false}
					  keyBoardControl={true}
					  transitionDuration={500}
					  containerClass="carousel-container"
					  deviceType={this.props.deviceType}
					>
						{this.props.scholarships.map(scholarship => {
							const url = `/scholarship/${scholarship._id}`;

							// calculate remaining applying days
							let today;
							let endDate
							let message;
							if (scholarship.endDate) {
								today = new Date().getTime() / (24 * 3600 * 1000);
								endDate = new Date(scholarship.endDate).getTime() / (24 * 3600 * 1000);
								const diffTime = Math.floor(endDate - today);
								message = `${diffTime} days left`;
							}
							else {
								message = "no limit registration date";
								today = 0;
								endDate = 0;
							}

							return (
								<Card className="ml-md-3 mr-md-3" key={scholarship._id}>
									<CardHeader className="d-flex flex-row club-card-header">
										<Col xs="7" className="club-header-text">
											<p className="club-type">{scholarship.scholarshipName}</p>
										</Col>
										<Col xs="5" className="club-header-btn">
											<Link to={url}>
												<Button>Detail</Button>
											</Link>
										</Col>
									</CardHeader>
									<CardBody className="d-flex flex-row club-card-body">
										<div className="d-flex flex-column club-card-body-text progress-container">
											<p>{scholarship.appliedAthlete.length} applied</p>
											<div className="text-right progress-days-left">{message}</div>
											<Progress value={today} max={endDate} className="time-progress"></Progress>
										</div>
									</CardBody>
								</Card>
							)
						})}
					</Carousel>
				</div>
			</div>
		);
	}	
}