import React from 'react';
import { Row, Col, Button, Alert } from 'reactstrap';
import { Link } from 'react-router-dom';
import '../../Athlete/AthleteDetail/AthleteProfile.css';
import './ClubProfile.css';

const ClubProfile = props => {

	// Below code needs to be modified in order to not allow just any login user to have access to update button
	let clubBtn;
	if (sessionStorage.getItem('hasLoggedIn') && sessionStorage.getItem('id') === props.id) {
		clubBtn = (
			<Row className="pt-3 athlete-link-container pl-md-5 mb-5 pr-md-5 mr-xl-auto width-x1">
				<div className="club-profile-btn mr-auto ml-auto pb-3 mr-md-2">
					<Link to="/club/form">
						<Button color="primary" className="mr-auto mb-1 mr-md-2 update-btn">UPDATE PROFILE</Button>
					</Link>
					<Link to="/scholarship/form">
						<Button className="submit-btn ml-auto mt-1 mt-md-0 mb-4 mb-md-1 ml-md-2">
							POST A SCHOLARSHIP
						</Button>
					</Link>
				</div>
			</Row>
		);
	}

	return (
		<div className="container-fluid top-container club-profile-container">
			<Row className="pt-4 pl-md-5 pr-md-5 mr-xl-auto ml-xl-auto width-x1">
				<Col xs="6" md="3" lg="2">
					<img src={props.club.clubLogo} 
							alt="athlete profile" className="athlete-media"/>
				</Col>
				<Col xs="6" md="8" lg="9" className="athlete-detail-group">
					<p className="athlete-name m-0 mb-3">{props.club.clubName}</p>
					<p className="athlete-detail m-0">{props.club.clubAddress}</p>
					<p className="athlete-detail m-0">{props.club.clubPhone}</p>
				</Col>
			</Row>
			<Row className="d-flex flex-row justify-content-center w-100 ml-auto mr-auto mt-4">
				<Alert color="info" className="ml-auto mr-auto club-about-alert">
					{props.club.clubAbout}
				</Alert>
			</Row>
			{clubBtn}
		</div>
	)
}

export default ClubProfile;