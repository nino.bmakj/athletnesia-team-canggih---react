import React from 'react';
import {
	Row,
	Card,
	CardHeader,
	CardBody,
	UncontrolledAlert
} from 'reactstrap';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import '../../Athlete/AthleteDetail/ClubApplied.css';
import '../../Athlete/AthleteDetail/ClubRecommend.css';
import './AthleteRecommend.css';


export default class AthleteRecommend extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			athletes: []
		}
	}

	componentDidMount() {
		fetch(`${process.env.REACT_APP_BACKEND_API}/athletes`, {
			method: 'GET'
		})
		.then(response => response.json())
		.then(result => {
			this.setState((state) => ({
				athletes: [...state.athletes, ...result.data]
			}));
		})
		.catch(err => err);
	}

	render() {
		const responsive1 = {
			xl: {
				breakpoint: {max: 3000, min: 1200},
				items: 3,
				partialVisibilityGutter: 15
			},
			lg: {
				breakpoint: {max: 1199, min: 992},
				items: 2,
				partialVisibilityGutter: 100
			},
			md: {
				breakpoint: {max: 991, min: 768},
				items: 2,
				partialVisibilityGutter: 35
			},
			sm: {
				breakpoint: {max: 767, min: 576},
				items: 2
			},
			xs: {
				breakpoint: {max: 576, min: 0},
				items: 1
			}
		}
		/*
		const responsive2 = {
			xl: {
				breakpoint: {max: 3000, min: 1200},
				items: 1
			},
			lg: {
				breakpoint: {max: 1199, min: 992},
				items: 1
			},
			md: {
				breakpoint: {max: 991, min: 768},
				items: 1
			},
			sm: {
				breakpoint: {max: 767, min: 576},
				items: 1
			},
			xs: {
				breakpoint: {max: 576, min: 0},
				items: 1
			} 
		}

		let responsiveType;
		let alert;
		if (this.athletes.length > 1) {
			responsiveType = responsive1
		}
		else if (this.athletes.length === 1) {
			responsiveType = responsive2;
		}
		else {
			responsiveType = responsive2;
			alert = (
				<div className="d-flex flex-column justify-content-center align-items-center mt-3 mt-md-5">
					<UncontrolledAlert color="warning">
						There are no athletes recommendation, yet.
					</UncontrolledAlert>
				</div>
			)
		}
		*/
		let alert;
		if (this.state.athletes.length === 0) {
			alert = (
				<div className="d-flex flex-column justify-content-center align-items-center mt-3 mt-md-5">
					<UncontrolledAlert color="warning">
						There are no athletes recommendation, yet.
					</UncontrolledAlert>
				</div>
			)
		}

		const titleStr = this.props.titleStr !== undefined ? this.props.titleStr : "RECOMMENDED Athlete for you";

		return (
			<div className="container-fluid recommend-container">
				<Row className="club-row-1 d-flex row-1-container">
					<p className="ml-md-4">{titleStr}</p>
					<form id="search-form2" className="d-flex flex-row-reverse justify-content-between mr-md-4">
						<input type="text" placeholder="Cari Atlet"/>
						<button type="submit"><i className="fas fa-search"></i></button>
					</form>
				</Row>
				<div className="club-row-2">
					{alert}
					<Carousel
						partialVisible={true}
						responsive={responsive1}
						swipeable={true}
						draggable={false}
						infinite={true}
						showDots={true}
						keyBoardControl={true}
						transitionDuration={500}
						containerClass="carousel-container"
						deviceType={this.props.deviceType}
					>
						{this.state.athletes.map(athlete => {

							// set first character uppercase
							const firstName = athlete.userId.firstName.toLowerCase().charAt(0).toUpperCase() + athlete.userId.firstName.slice(1);
							const lastName = athlete.userId.lastName.toLowerCase().charAt(0).toUpperCase() + athlete.userId.lastName.slice(1);
							const name = `${firstName} ${lastName}`;

							// calculating age
							const today = new Date();
							const born = new Date(athlete.bornDate);

							let diffYear = today.getFullYear() - born.getFullYear();
							const diffMonth = today.getMonth() - born.getMonth();
							const diffDate = today.getDate() - born.getDate();

							if (diffMonth < 0) diffYear -= 1; // if current month hasn't passed born month
							if (diffMonth === 0 && diffDate < 0) diffYear -= 1; // if it's already in born month but hasn't passed the born date

							const age = `${diffYear} tahun`;

							// athlete status
							let status;
							if (athlete.sportId === undefined) {
								status = `${athlete.status}`;
							} else {
								status = `${athlete.status} ${athlete.sportId.sportName}`;
							}

							return (
								<Card className="ml-md-3 mr-md-3" key={athlete._id}>
									<CardHeader className="d-flex flex-column ml-auto mr-auto align-items-center club-card-header border-none">
										<img src={athlete.profilePicture} 
											alt="profile" className="club-media"/>
										<div className="d-flex flex-column justify-content-start align-items-center ml-auto mr-auto mt-3 line-height-05">
											<p className="athlete-recommend-name">{name}</p>
											<p className="athlete-recommend-text">{age}</p>
											<p className="athlete-recommend-text">{status}</p>
										</div>
									</CardHeader>
									<CardBody className="d-flex flex-column club-card-body pt-0">
										<p className="pl-3 athlete-recommend-text">Skill</p>
										<div className="d-flex flex-column">
											<ul>
												{athlete.skillId.map(skill => {
													return (
														<li className="skill-point" key={skill._id}>{skill.skill}</li>
													)
												})}
											</ul>
										</div>
									</CardBody>
								</Card>
							)
						})}
					</Carousel>
				</div>
			</div>
		);
	}
}