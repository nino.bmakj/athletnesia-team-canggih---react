import React, { useState, useEffect } from 'react';
import {
	Container,
	Col,
	Button,
	Form,
	FormGroup,
	Label,
	Input
} from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';
import queryString from 'query-string';
import '../../Athlete/AthleteForm/index.css';

const ClubForm = props => {
	const [firstName, setFirstName] = useState('');
	const [sports, setSports] = useState([]);
	const [clubLogo, setClubLogo] = useState('');
	const [clubSportId, setClubSportId] = useState('');
	const [clubSport, setClubSport] = useState('');
	const [clubAddress, setClubAddress] = useState('');
	const [clubPhone, setClubPhone] = useState('');
	const [clubEmail, setClubEmail] = useState('');
	const [clubAbout, setClubAbout] = useState('');
	const [isSubmited, setIsSubmited] = useState(false);
	const parsed = queryString.parse(props.location.search);
	let sportVal;
	let sportPlaceholder;
	let button;

	useEffect(() => {
		Promise.all([
			fetch(`${process.env.REACT_APP_BACKEND_API}/user/${sessionStorage.getItem('userId')}`, {method: 'GET'}),
			fetch(`${process.env.REACT_APP_BACKEND_API}/club/${sessionStorage.getItem('id')}`, {method: 'GET'}),
			fetch(`${process.env.REACT_APP_BACKEND_API}/sports`, {method: 'GET'})
		])
		.then(allResponses => {
			return [allResponses[0].json(), allResponses[1].json(), allResponses[2].json()];
		})
		.then(results => {
			const userResult = results[0];
			const clubResult = results[1];
			const sportsResult = results[2];

			userResult.then(data => {
				setFirstName(data.data.firstName);
			})
			.catch(err => err);

			clubResult.then(data => {
				setClubLogo(data.data.clubLogo);
				setClubSportId(data.data.sportId._id);
				setClubSport(data.data.sportId.sportName)
				setClubAddress(data.data.clubAddress);
				setClubPhone(data.data.clubPhone);
				setClubEmail(data.data.clubEmail);
				setClubAbout(data.data.clubAbout);
			})
			.catch(err => err);

			sportsResult.then(data => {
				// get all documents and its properties
				setTimeout(() => setSports(data.data), 1000);
			})
			.catch(err => err);

		})
		.catch(err => err);

	}, []);

	function handleFirstNameChange(event) {
		if (firstName !== event.target.value) setFirstName(event.target.value);
	}

	function handleSportChange(event) {
		if (clubSport !== event.target.value) setClubSportId(event.target.value);
	}

	function handleAddressChange(event) {
		if (clubAddress !== event.target.value) setClubAddress(event.target.value);
	}

	function handlePhoneChange(event) {
		if (clubPhone !== event.target.value) setClubPhone(event.target.value);
	}

	function handleEmailChange(event) {
		if (clubEmail !== event.target.value) setClubEmail(event.target.value);
	}

	function handleLogoChange(event) {
		const formData = new FormData();

		formData.append('file', event.target.files[0]);
		formData.append('upload_preset', process.env.REACT_APP_CLOUDINARY_UPLOAD_PRESET);

		fetch(process.env.REACT_APP_CLOUDINARY_UPLOAD_URL, {
			method: 'POST',
			body: formData
		})
		.then(response => response.json())
		.then(data => {
			setClubLogo(data.secure_url);
		})
		.catch(err => err);
	}

	function handleAboutChange(event) {
		if (clubAbout !== event.target.value) setClubAbout(event.target.value);
	}

	function handleClickSubmit(event) {
		Promise.all([
			// this will update to /club/:id
			fetch(`${process.env.REACT_APP_BACKEND_API}/club/${sessionStorage.getItem('id')}`, {
				method: 'PUT',
				cache: 'no-cache',
				credentials: 'same-origin',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': sessionStorage.getItem('token')
				},
				body: JSON.stringify({
					sportId: clubSportId,
					clubAddress: clubAddress,
					clubPhone: clubPhone,
					clubEmail: clubEmail,
					clubAbout: clubAbout,
					clubLogo: clubLogo
				})
			}),
			// this will update to /user/:id
			fetch(`${process.env.REACT_APP_BACKEND_API}/user/${sessionStorage.getItem('userId')}`, {
				method: 'PUT',
				cache: 'no-cache',
				credentials: 'same-origin',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': sessionStorage.getItem('token')
				},
				body: JSON.stringify({
					firstname: firstName
				})
			})
		])
		.then(allResponses => {
			return [allResponses[0].json(), allResponses[1].json()];
		})
		.then(results => {
			const clubResult = results[0];
			const userResult = results[1];

			clubResult
				.then(data => sessionStorage.setItem('clubMessage', data.message))
				.catch(err => err);

			userResult
				.then(data => sessionStorage.setItem('userMessage', data.message))
				.catch(err => err);

			setIsSubmited(true);

			window.location.reload(true);
		})
	}

	if (clubSport === '') {
		sportVal = '';
		sportPlaceholder = '--Pilih salah satu--';
	}
	else {
		sportVal = clubSportId;
		sportPlaceholder = clubSport;
	}

	if (parsed.justSignedUp === 'true') {
		button = <Button className="athlete-form-btn back-btn" hidden>Back</Button>
	} else {
		const url = `/club/${sessionStorage.getItem('id')}`;
		button = (
			<Link to={url}>
				<Button className="athlete-form-btn back-btn">Back</Button>
			</Link>
		)
	}

	let url = `/club/${sessionStorage.getItem('id')}`;
	if (isSubmited) {
		return <Redirect to={url}></Redirect>
	}

	return (
		<Container className="mt-3 mb-4 pl-md-4 pr-md-4">
			<p className="athlete-form-title">Profile Club</p>
			<Form encType="multipart/form-data">
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="clubName">Nama Klub</Label>
					</Col>
					<Col md="8">
						<Input type="text" name="clubname" id="clubName" onChange={handleFirstNameChange} 
							placeholder={firstName} value={firstName} required></Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="sportOptions">Bidang</Label>
					</Col>
					<Col md="8">
						<Input type="select" name="sportoptions" id="sportOptions" onChange={handleSportChange} required>
							<option value={sportVal}>{sportPlaceholder}</option>
							{sports.map(sport => {
								return <option key={sport._id} value={sport._id}>{sport.sportName}</option>
							})}
						</Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="clubAddress">Alamat</Label>
					</Col>
					<Col md="8">
						<Input type="textarea" name="clubaddress" id="clubAddress" 
							placeholder={clubAddress} onChange={handleAddressChange} required></Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="clubPhone">No Telepon</Label>
					</Col>
					<Col md="8">
						<Input type="tel" name="clubphone" id="clubPhone" placeholder={clubPhone} onChange={handlePhoneChange} required></Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="clubEmail">Email</Label>
					</Col>
					<Col md="8">
						<Input type="email" name="clubemail" id="clubEmail" placeholder={clubEmail} onChange={handleEmailChange}></Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="clubLogo">Foto Profil</Label>
					</Col>
					<Col md="8">
						<Input type="file" name="clublogo" id="clubLogo" onChange={handleLogoChange}></Input>
					</Col>
				</FormGroup>
				<FormGroup className="d-flex athlete-form-group">
					<Col md="4">
						<Label for="clubAbout">Deskripsi Klub</Label>
					</Col>
					<Col md="8">
						<Input type="textarea" name="clubabout" id="clubAbout" placeholder={clubAbout} onChange={handleAboutChange}></Input>
					</Col>
				</FormGroup>
				<div className="d-flex athlete-btn-container">
					{button}
					<Button className="athlete-form-btn athlete-submit ml-3" onClick={handleClickSubmit}>Submit</Button>
				</div>
			</Form>
		</Container>
	)
}

export default ClubForm;