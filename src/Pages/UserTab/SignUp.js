import React, { useState } from 'react';
import {
	Col,
	Button,
	Form,
	FormGroup,
	Input,
	ButtonGroup,
	Row,
	Label,
	Alert
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import './UserTab.css';


function SignUp(props) {
	const [selected, setSelected] = useState(1);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isAgreed, setIsAgreed] = useState(false);
	const [entity, setEntity] = useState('athlete');
	const [isLoggedIn, setIsLoggedIn] = useState(false);
	const [visible, setVisible] = useState(false);
	const [isInvalid, setIsInvalid] = useState(false);

	const onDismiss = () => setVisible(false);

	if (isLoggedIn) {
		const url = "/user?tab=signIn&justSignedUp=true";
		return (
			<Redirect to={url}></Redirect>
		)
	}

	let nameField;
	if (entity === 'club') {
		nameField = (
			<FormGroup row className="d-flex name-form">
				<Col md={12}>
					<Label for="clubName" hidden>Nama club</Label>
					<Input invalid={isInvalid} type="text" name="corpname" id="clubName" placeholder="Nama klub" onChange={handleFirstNameChange} required></Input>
				</Col>
			</FormGroup>
		)
	}
	else {
		nameField = (
			<FormGroup row className="d-flex name-form">
				<Col md={6}>
					<Label for="firstName" hidden>Nama depan</Label>
					<Input invalid={isInvalid} type="text" name="firstname" id="firstName" placeholder="Nama depan" onChange={handleFirstNameChange} required></Input>
				</Col>
				<Col md={6}>
					<Label for="lastName" hidden>Nama belakang</Label>
					<Input type="text" name="lastname" id="lastName" placeholder="Name belakang" onChange={handleLastNameChange}></Input>
				</Col>		
			</FormGroup>
		)
	}

	return (
		<div className="container form-container">
			<Alert color="danger" isOpen={visible} toggle={onDismiss} className="text-center">
				Please fill in the required field
			</Alert>

			<ButtonGroup className="d-flex radio-btn">
				<Button onClick={handleAthleteClick} color="primary" active={selected === 1} className="athlete-sign-btn">
					Calon Atlet
				</Button>
				<Button onClick={handleClubClick} color="primary" active={selected === 2} className="club-sign-btn">
					Klub Atlet
				</Button>
			</ButtonGroup>
			<Form>
				{nameField}
				<FormGroup row className="email-form">
					<Label for="signUpEmail" hidden>Email</Label>
					<Input invalid={isInvalid} type="email" name="email" id="signUpEmail" placeholder="Email" onChange={handleEmailChange} required></Input>
				</FormGroup>
				<FormGroup row className="password-form">
					<Label for="signUpPassword" hidden>Password*</Label>
					<Input invalid={isInvalid} type="password" name="password" id="signUpPassword" placeholder="Password" onChange={handlePasswordChange} required></Input>
				</FormGroup>
				<FormGroup check>
					<Label for="checkbox1" hidden>Agreed</Label>
					<Input invalid={isInvalid} type="checkbox" id="checkbox1" onChange={handleAgreedCheck} required></Input>
					{/* eslint-disable-next-line*/}
					<p>Menyetujui <a href="#">peraturan</a> dan <a href="#">persyaratan</a></p>
				</FormGroup>
				<Button className="submit-btn" color="#ffb24d" onClick={handleSubmitClick} block>DAFTAR</Button>
			</Form>
			<Row className="d-flex flex-row justify-content-center mt-3">
				<p>Sudah punya akun? <Link to="/user?tab=signIn">Masuk</Link></p>
			</Row>
			<Row className="d-flex social-media-btn">
				<Col md="6">
					<Button className="google-btn mt-1 mb-1" color="#f34747" block>Daftar dengan Google</Button>
				</Col>
				<Col md="6">
					<Button className="facebook-btn mt-1 mb-1" color="#235bbb" block>Daftar dengan Facebook</Button>
				</Col>
			</Row>
		</div>
	);

	function handleAthleteClick() {
		setSelected(1);
		setEntity('athlete');
	}

	function handleClubClick() {
		setSelected(2);
		setEntity('club');
	}

	function handleFirstNameChange(event) {
		setFirstName(event.target.value);
	}

	function handleLastNameChange(event) {
		setLastName(event.target.value);
	}

	function handleEmailChange(event) {
		setEmail(event.target.value);
	}

	function handlePasswordChange(event) {
		setPassword(event.target.value);
	}

	function handleAgreedCheck() {
		setIsAgreed(true);
	}

	function handleSubmitClick(event) {
		event.preventDefault();

		const found = firstName === '' || email === '' || password === '' || isAgreed === false;

		if (!found) {
			fetch(`${process.env.REACT_APP_BACKEND_API}/sign-up?joinAs=${entity}`, {
				method: 'POST',
				mode: 'cors',
				cache: 'no-cache',
				credentials: 'same-origin',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password
				})
			})
			.then(response => {
				setIsLoggedIn(true);
				setIsInvalid(false);
				setVisible(false);

				//sessionStorage.setItem('justSignedUp', true);
				sessionStorage.setItem('registeredAs', entity);
				return response.json();
			})
			.catch(err => err);
		} 
		else {
			setVisible(true);
			setIsInvalid(true)
		}
	}
}

export default SignUp;