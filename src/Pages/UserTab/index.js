import React, { useState } from 'react';
import {
	Nav,
	NavItem,
	NavLink,
	TabContent,
	TabPane
} from 'reactstrap';
import SignUp from './SignUp';
import Login from './Login';
import './index.css';
import classnames from 'classnames';
import queryString from 'query-string';

const UserTab = (props) => {
	const [activeTab, setActiveTab] = useState('1');

	const toggle = tab => {
		if(activeTab !== tab) setActiveTab(tab);
	}

	const parsed = queryString.parse(props.location.search);

	if (parsed.tab === 'signIn') {
		if (parsed.justSignedUp === 'true') {
			props.location.search = '?justSignedUp=true';
		}
		else {
			props.location.search = '';
		}

		toggle('2');
	}
	else if (parsed.tab === 'signUp') {
		props.location.search = '';
		toggle('1');
	}

	return(
		<div className="container-fluid user-tab-container">
			<Nav tabs>
				<NavItem className="col-6 text-center">
					<NavLink className={classnames({active: activeTab === '1'})}
					onClick={() => toggle('1')}>
						Sign Up
					</NavLink>
				</NavItem>
				<NavItem className="col-6 text-center">
					<NavLink className={classnames({active: activeTab === '2'})}
					onClick={() => toggle('2')}>
						Log In
					</NavLink>
				</NavItem>
			</Nav>
			<TabContent activeTab={activeTab}>
				<TabPane tabId="1">
					<SignUp></SignUp>
				</TabPane>
				<TabPane tabId="2">
					<Login parsed={parsed}></Login>
				</TabPane>
			</TabContent>
		</div>
	)
}

export default UserTab;