import React, { useState } from 'react';
import {
	Col,
	Button,
	ButtonGroup,
	Form,
	FormGroup,
	Input,
	Row,
	Label,
	Alert
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import './UserTab.css';


const Login = (props) => {
	const [selected, setSelected] = useState(1);
	const [email, setEmail] = useState(null);
	const [password, setPassword] = useState(null);
	const [entity, setEntity] = useState('athlete');
	const [isAuthenticated, setIsAuthenticated] = useState(false);
	const [incorrectMessage, setIncorrectMessage] = useState(null);
	const [visible, setVisible] = useState(false);
	const [isInvalid, setIsInvalid] = useState(false);
	const [justSignedUp, setJustSignedUp] = useState(false);
	let url;

	const onDismiss = () => setVisible(false);

	if (isAuthenticated) {
		if (entity === 'athlete') {
			if (justSignedUp) {url = "/athlete/form?justSignedUp=true"}
			else {url = `/athlete/${sessionStorage.getItem('id')}`}
		} 
		else if (entity === 'club') {
			if (justSignedUp) {url = "/club/form?justSignedUp=true"} 
			else {url = `/club/${sessionStorage.getItem('id')}`}
		}

		return (
			<Redirect to={url}></Redirect>
		)
	}

	return (
		<div className="container form-container">
			<Alert color="danger" isOpen={visible} toggle={onDismiss} className="text-center">{incorrectMessage}</Alert>

			<ButtonGroup className="d-flex radio-btn">
				<Button onClick={handleAthleteClick} color="primary" active={selected === 1} className="athlete-sign-btn">
					Calon Atlet
				</Button>
				<Button onClick={handleClubClick} color="primary" active={selected === 2} className="club-sign-btn">
					Klub Atlet
				</Button>
			</ButtonGroup>
			<Form className="mt-2">
				<FormGroup row className="d-flex email-form">
					<Label for="signInEmail" hidden>Email</Label>
					<Input invalid={isInvalid} type="email" name="email" id="signInEmail" placeholder="Email" onChange={handleEmailChange} required></Input>
				</FormGroup>
				<FormGroup row className="password-form">
					<Label for="signInPassword" hidden>Email</Label>
					<Input invalid={isInvalid} type="password" name="password" id="signInPassword" placeholder="Password" onChange={handlePasswordChange} required></Input>
				</FormGroup>
				<Button className="submit-btn" color="#ffb24d" onClick={handleSubmitClick} block>LOGIN</Button>
			</Form>
			<Row className="d-flex flex-row justify-content-center mt-3">
				<p>Belum punya akun? <Link to="/user?tab=signUp">Daftar</Link></p>
			</Row>
			<Row className="d-flex social-media-btn">
				<Col md="6">
					<Button className="google-btn mt-1 mb-1" color="#f34747" block>Login dengan Google</Button>
				</Col>
				<Col md="6">
					<Button className="facebook-btn mt-1 mb-1" color="#235bbb" block>Login dengan Facebook</Button>
				</Col>
			</Row>
		</div>
	);

	function handleAthleteClick() {
		setSelected(1);
		setEntity('athlete');
	}

	function handleClubClick() {
		setSelected(2);
		setEntity('club');
	}

	function handleEmailChange(event) {
		setEmail(event.target.value);
	}

	function handlePasswordChange(event) {
		setPassword(event.target.value);
	}

	function handleSubmitClick() {
		const found = email === null || password === null;

		if (!found) {
			fetch(`${process.env.REACT_APP_BACKEND_API}/${entity}/login`, {
				method: 'POST',
				cache: 'no-cache',
				credentials: 'same-origin',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(response => response.json())
			.then(result => {
				console.log(result)
				if (result.success) {
					sessionStorage.setItem('token', result.data.token);
					sessionStorage.setItem('hasLoggedIn', true);
					sessionStorage.setItem('userId', result.data.userId);

					if (entity === 'athlete') {
						sessionStorage.setItem('id', result.data.athleteId);
						sessionStorage.setItem('registeredAs', 'athlete');
					} 
					else if (entity === 'club') {
						sessionStorage.setItem('id', result.data.clubId);
						sessionStorage.setItem('registeredAs', 'club');
					}
					
					return result.success;

				} 
				else {
					setIncorrectMessage(result.message);
					setVisible(true);

					return result.success
				}
			})
			.then((pass) => {
				if (pass) {
					if (props.parsed.justSignedUp === 'true') setJustSignedUp(true);
					setIsInvalid(false);
					setIsAuthenticated(true);
					window.location.reload(true);
				}
			})
			.catch(err => err);
		} 
		else {
			setIncorrectMessage('please fill in all required field');
			setVisible(true);
			setIsInvalid(true);
		}
	}
}

export default Login;