import React, { useState, useEffect } from 'react';
import {
	Container,
	Col,
	Row,
	Button,
	Form,
	FormGroup,
	Label,
	Input
} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import queryString from 'query-string';
import '../../Athlete/AthleteForm/index.css';
import './index.css';

const ScholarshipForm = props => {
	const [scholarshipName, setScholarshipName] = useState('');
	const [quota, setQuota] = useState('');
	const [description, setDescription] = useState('');
	const [prerequisites, setPrerequisites] = useState([]);
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	const [isSubmited, setIsSubmited] = useState(false);
	const parsed = queryString.parse(props.location.search);
	let fetchUrl;
	let submitMethod;

	useEffect(() => {
		if (parsed.isUpdating === true) {
			fetch(`${process.env.REACT_APP_BACKEND_API}/scholarship/${parsed.scholarshipId}`, {method: 'GET'})
				.then(response => response.json())
				.then(result => {
					setScholarshipName(result.data.scholarshipName);
					setQuota(result.data.quota);
					setDescription(result.data.description);
					setPrerequisites(result.data.prerequisites);
					setStartDate(result.data.startDate);
					setEndDate(result.data.endDate);
				})
				.catch(err => alert(err.message));
		}
	}, []); //eslint-disable-line

	if (parsed.isUpdating === true) {
		fetchUrl = `${process.env.REACT_APP_BACKEND_API}/scholarship/${parsed.scholarshipId}`;
		submitMethod = 'PUT';
	} 
	else if (parsed.isUpdating === false) {
		const url = `${process.env.REACT_APP_BACKEND_API}/scholarship/form`;
		return <Redirect to={url}></Redirect>
	} 
	else {
		fetchUrl = `${process.env.REACT_APP_BACKEND_API}/add-scholarship`;
		submitMethod = 'POST';
	}

	const handleTitleChange = event => {
		if (scholarshipName !== event.target.value) setScholarshipName(event.target.value);
	}

	const handleQuotaChange = event => {
		if (quota !== event.target.value) setQuota(event.target.value);
	}

	const handleDescriptionChange = event => {
		if (description !== event.target.value) setDescription('');
	}

	const handlePrerequisitesChange = event => {
		const arrAfter = [...prerequisites, ...event.target.value];
		if (prerequisites !== arrAfter) setPrerequisites([...prerequisites, ...event.target.value]);
	}

	const handleStartDateChange = event => {
		if (startDate !== event.target.value) setStartDate(event.target.value);
	}

	const handleEndDateChange = event => {
		if (endDate !== event.target.value) setEndDate(event.target.value);
	}

	const handleSubmitClick = () => {
		fetch(fetchUrl, {
			method: submitMethod,
			headers: {
				'Content-Type': 'application/json',
				'Authorization': sessionStorage.getItem('token')
			},
			body: JSON.stringify({
				scholarshipName: scholarshipName,
				quota: quota,
				description: description,
				prerequisites: prerequisites,
				startDate: startDate,
				endDate: endDate
			})
		})
		.then(response => response.json())
		.then(result => setIsSubmited(true))
		.catch(err => alert(err.message));
	}

	if (isSubmited) {
		let url;
		parsed.isUpdating === true ? url = `/${parsed.next}` : url = `/club/${sessionStorage.getItem('id')}`;

		return <Redirect to={url}></Redirect>
	}

	return (
			<Container className="mt-3 mb-4 pl-md-4 pr-md-4">
				<p className="athlete-form-title">Scholarship Form</p>
				<Form>
					<FormGroup className="d-flex athlete-form-group">
						<Col md="4">
							<Label for="scholarshipTitle">Judul</Label>
						</Col>
						<Col md="8">
							<Input type="text" name="scholarshiptitle" id="scholarshipTitle" 
								onChange={handleTitleChange} value={scholarshipName} placeholder={scholarshipName} required></Input>
						</Col>
					</FormGroup>
					<FormGroup className="d-flex athlete-form-group">
						<Col md="4">
							<Label for="scholarshipQuota">Kuota</Label>
						</Col>
						<Col md="8">
							<Input type="number" name="scholarshipquota" id="scholarshipQuota" 
								onChange={handleQuotaChange} value={quota} placeholder={quota} required></Input>
						</Col>
					</FormGroup>
					<FormGroup className="d-flex athlete-form-group">
						<Col md="4">
							<Label for="scholarshipDesc">Deskripsi</Label>
						</Col>
						<Col md="8">
							<Input type="textarea" name="scholarshipdesc" id="scholarshipDesc" 
								onChange={handleDescriptionChange} value={description} placeholder={description}></Input>
						</Col>
					</FormGroup>
					<FormGroup className="d-flex athlete-form-group">
						<Col md="4">
							<Label for="scholarshipPrerequisite">Syarat</Label>
						</Col>
						<Col md="8">
							<Input type="textarea" name="scholarshipprerequisite" id="scholarshipPrerequisite" 
								onChange={handlePrerequisitesChange} value={prerequisites} placeholder={prerequisites}></Input>
						</Col>
					</FormGroup>
					<FormGroup className="d-flex flex-column">
						<Row>
							<p className="scholarship-date-text mt-2">Tanggal Berlaku</p>
						</Row>
						<Row>
							<Col md="6" className="d-flex date-from">
								<Col md="4">
									<Label for="dateStart">Dari</Label>
								</Col>
								<Col md="8">
									<Input type="date" name="datestart" id="dateStart" 
										onChange={handleStartDateChange} value={startDate} placeholder={startDate} required></Input>
								</Col>
							</Col>
							<Col md="6" className="d-flex date-to">
								<Col md="4">
									<Label for="dateTo">Hingga</Label>
								</Col>
								<Col md="8">
									<Input type="date" name="dateto" id="dateTo" 
										onChange={handleEndDateChange} value={endDate} placeholder={endDate}></Input>
								</Col>
							</Col>
						</Row>
					</FormGroup>
					<div className="d-flex athlete-btn-container">
						<Button className="athlete-form-btn back-btn">Back</Button>
						<Button className="athlete-form-btn athlete-submit ml-3" onClick={handleSubmitClick}>Submit</Button>
					</div>
				</Form>
			</Container>
	)
}

export default ScholarshipForm;