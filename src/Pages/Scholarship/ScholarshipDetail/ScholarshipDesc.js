import React, { useState, useEffect } from 'react';
import { Row, Card, CardHeader, CardBody } from 'reactstrap';
import './ScholarshipDesc.css';
import './ScholarshipTop.css';

const ScholarshipDesc = props => {
	const [description, setDescription] = useState('');
	const [prerequisites, setPrerequisites] = useState([]);
	const [skills, setSkills] = useState([]); //eslint-disable-line

	useEffect(() => {
		fetch(`${process.env.REACT_APP_BACKEND_API}/scholarship/${props.id}`, {method: 'GET'})
			.then(response => response.json())
			.then(result => {
				setDescription(result.data.description);
				setPrerequisites(result.data.prerequisites);
				//setSkills(result.data.skillId.skill);
			})
			.catch(err => alert(err.message));
	}, []); //eslint-disable-line

	return (
		<div className="container mb-4">
			<Row>
				<Card className="ml-md-3 mr-md-3 width-60">
					<CardHeader className="scholarship-card-header">Deskripsi</CardHeader>
					<CardBody>
						<p className="color-949494">{description}</p>
					</CardBody>
				</Card>
				<Card className="ml-md-3 mr-md-3 mt-4 mt-lg-4 mt-xl-0 container-fluid pl-0 pr-0 width-30">
					<CardHeader className="scholarship-card-header">Skill</CardHeader>
					<CardBody>
						<ul className="d-flex flex-row flex-wrap pl-0 color-949494 text-center">
							{skills.map(skill => {
								return (
									<li className="skill-required ml-1 mr-1 mt-1 pl-2 pr-2 border-radius-50" key={skill._id}>
										{skill}
									</li>
								)
							})}
						</ul>
					</CardBody>
				</Card>
				<Card className="ml-md-3 mr-md-3 mt-4 container-fluid pl-0 pr-0 width-60">
					<CardHeader className="scholarship-card-header">Persyaratan</CardHeader>
					<CardBody>
						<ul className="color-949494">
							{prerequisites.map(prerequisite => {
								return <li className="list-type-disc">{prerequisite}</li>
							})}
						</ul>
					</CardBody>
				</Card>
			</Row>
		</div>
	)
}

export default ScholarshipDesc;