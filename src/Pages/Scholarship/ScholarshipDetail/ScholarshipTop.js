import React, { useState, useEffect } from 'react';
import { Row, Col, Button, Alert } from 'reactstrap';
import { Link } from 'react-router-dom';
import '../../Athlete/AthleteDetail/AthleteProfile.css';
import './ScholarshipTop.css';

const ScholarshipTop = props => {
	const [scholarshipName, setScholarshipName] = useState('');
	const [quota, setQuota] = useState('');
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	const [clubId, setClubId] = useState('');
	const [clubLogo, setClubLogo] = useState('');
	const [clubName, setClubName] = useState('');
	const [clubSport, setClubSport] = useState('');
	const [clubAddress, setClubAddress] = useState('');
	const [clubPhone, setClubPhone] = useState(''); //eslint-disable-line
	const [appliedAthlete, setAppliedAthlete] = useState([]);
	const [isApplied, setIsApplied] = useState(false);
	const dateFrom = new Date(startDate);
	const dateTo = new Date(endDate);
	const dateFromStr = `${dateFrom.getDate()}-${dateFrom.getMonth() + 1}-${dateFrom.getFullYear()}`;
	const dateToStr = `${dateTo.getDate()}-${dateTo.getMonth() + 1}-${dateTo.getFullYear()}`;
	const periodStr = `${dateFromStr} sampai ${dateToStr}`;

	useEffect(() => {
		fetch(`${process.env.REACT_APP_BACKEND_API}/scholarship/${props.id}`, {method: 'GET'})
			.then(response => response.json())
			.then(result => {
				setScholarshipName(result.data.scholarshipName);
				setQuota(result.data.quota);
				setStartDate(result.data.startDate);
				setEndDate(result.data.endDate);
				setClubId(result.data.clubId._id);
				setClubLogo(result.data.clubId.clubLogo);
				setClubSport(result.data.clubId.sportId.sportName);
				setClubName(result.data.clubId.userId.firstName);
				setClubAddress(result.data.clubId.clubAddress);
				setAppliedAthlete(result.data.appliedAthlete);
				setClubPhone(result.data.clubId.clubPhone);

			})
			.catch(err => alert(err.message));
	}, []); //eslint-disable-line

	const handleApplyClick = () => {
		fetch(`${process.env.REACT_APP_BACKEND_API}/scholarship/${props.id}/apply`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': sessionStorage.getItem('token')
			}
		})
		.then(response => response.json())
		.then(result => setIsApplied(true))
		.catch(err => err);
	}

	let updateBtn;
	let applyBtn;
	// Below code is needed in order to not allow just any login user to have access to update button
	/* 
		Backend needs to be able to handle the situation when an athlete has applied to a scholarship and press
		apply button on an already applied scholarship page.
	*/
	if (sessionStorage.getItem('hasLoggedIn')) {
		// APPLY button will appear if user is an athlete and has not applied
		const hasApplied = appliedAthlete.find(athlete => athlete._id === sessionStorage.getItem('id'))
		// eslint-disable-next-line
		if (hasApplied !== undefined && sessionStorage.getItem('registeredAs') === 'athlete' || quota === appliedAthlete.length) {
			applyBtn = (
				<Button className="submit-btn ml-auto pl-5 pr-5 border-radius-50 d-none" onClick={handleApplyClick}>
					APPLY
				</Button>
			);
		}
		else if (sessionStorage.getItem('registeredAs') === 'club' && clubId !== sessionStorage.getItem('id')) {
			applyBtn = (
				<Button className="submit-btn ml-auto pl-5 pr-5 border-radius-50 d-none" onClick={handleApplyClick}>
					APPLY
				</Button>
			);
		}
		else {
			applyBtn = (
				<Button className="submit-btn ml-auto pl-5 pr-5 border-radius-50" onClick={handleApplyClick}>
					APPLY
				</Button>
			);
		}

		// Update Scholarship button will only appear if a logged in user is the scholarship provider
		if (sessionStorage.getItem('id') === clubId) {
			const url = `/athlete/form?isUpdating=true&&next=scholarship/${props.id}`;
			updateBtn = (
				<div className="d-flex flex-row justify-content-end mr-auto ml-auto pb-3 pr-md-5 width-xl">
					<Link to={url}>
						<Button color="primary">Update Scholarship</Button>
					</Link>
				</div>
			);
		}
	}

	// Alert will be triggered after a user has applied the scholarship
	let alert;
	if (isApplied) {
		alert = <Alert color="info">You have applied to this scholarship</Alert>;
	}

	// Check if quota has been fulfilled
	let quotaPlaceholder;
	if (quota === appliedAthlete.length || quota < appliedAthlete.length) {
		quotaPlaceholder = (
			<p className="font-size-20 mb-0 text-danger">Kuota sudah terpenuhi</p>
		)
	} else {
		quotaPlaceholder = (
			<p className="font-size-20 mb-0">Tersedia: {quota - appliedAthlete.length} orang</p>
		)
	}

	// Check if the current date has surpassed the end period of scholarship application
	let periodPlaceholder;
	const today = new Date();
	if (today > dateTo) {
		periodPlaceholder = (
			<p className="font-size-20 mb-0 text-danger">Periode pendaftaran sudah berakhir</p>
		)
	} else {
		periodPlaceholder = (
			<p className="font-size-20 mb-0">Periode: {periodStr}</p>
		)
	}

	return (
		<div className="container-fluid top-container">
			{alert}
			<Row className="pt-4 pl-md-5 pr-md-5 mr-xl-auto ml-xl-auto width-x1">
				<Col xs="6" md="3" lg="2">
					<img src={clubLogo} 
							alt="scholarship" className="athlete-media"/>
				</Col>
				<Col xs="6" md="8" lg="9" className="athlete-detail-group">
					<p className="athlete-name m-0 mb-3">{scholarshipName}</p>
					<p className="athlete-detail m-0">Cabang {clubSport}</p>
					<p className="athlete-detail m-0">{clubName}</p>
					<p className="athlete-detail m-0">{clubAddress}</p>
					<p className="athlete-detail m-0">{clubPhone}</p>
				</Col>
			</Row>
			<Row className="pt-3 athlete-link-container pl-3 pr-3 pl-md-5 pr-md-5 mr-xl-auto ml-xl-auto">
				<ul className="d-flex flex-column flex-md-row align-items-md-center ml-md-2 mr-md-2 athlete-link-group pl-0 pr-0">
					<li className="mr-3 border-0">
						{quotaPlaceholder}
					</li>
					<li className="mr-3 border-0">
					{periodPlaceholder}
					</li>
					<li className="mr-3 mt-3 mt-md-0 border-0 align-self-center">
						{applyBtn}
						{updateBtn}
					</li>
				</ul>
			</Row>
		</div>
	)
}

export default ScholarshipTop;