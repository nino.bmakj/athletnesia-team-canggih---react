import React from 'react';
import ScholarshipTop from './ScholarshipTop';
import ScholarshipDesc from './ScholarshipDesc';

const ScholarshipDetail = props => {
	return (
		<div>
			<ScholarshipTop id={props.match.params.id}></ScholarshipTop>
			<ScholarshipDesc id={props.match.params.id}></ScholarshipDesc>
		</div>
	)
}

export default ScholarshipDetail;