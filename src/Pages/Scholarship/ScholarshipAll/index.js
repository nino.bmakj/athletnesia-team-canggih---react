import React, { useState, useEffect } from 'react';
import { 
	Row,
	Card,
	CardHeader,
	CardBody,
	Col,
	Button,
	Alert
} from 'reactstrap';
import { Link } from 'react-router-dom';
import '../../Athlete/AthleteDetail/ClubApplied.css';
import '../ScholarshipAll/index.css';

const ScholarshipAll = props => {
	const [scholarships, setScholarships] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_BACKEND_API}/scholarships`, {
			method: 'GET'
		})
		.then(response => response.json())
		.then(result => setScholarships(result.data))

	}, []); //eslint-disable-line

	let alert;
	if (scholarships.length === 0) {
		alert = <Alert className="text-center" color="warning">There are no scholarships available right now :(.</Alert>
	}

	return (
		<div className="container-fluid width-75 ml-auto mr-auto mb-4">
			<Row className="club-row-1 pt-4 pl-4 pl-md-5 pl-xl-0 ml-xl-2">
				<p>Beasiswa yang tersedia</p>
			</Row>
			{alert}
			<Row className="justify-content-center">
				{scholarships.map(scholarship => {
					const url = `/scholarship/${scholarship._id}`

					return (
						<Card className="ml-md-3 mr-md-3 mt-3 mb-3" key={scholarship._id}>
							<CardHeader className="d-flex flex-row club-card-header">
								<Col xs="7" className="club-header-text">
									<p className="club-type">{scholarship.scholarshipName}</p>
								</Col>
								<Col xs="5" className="club-header-btn">
									<Link to={url}>
										<Button>Detail</Button>
									</Link>
								</Col>
							</CardHeader>
							<CardBody className="d-flex flex-row club-card-body">
								<img src={scholarship.clubId.clubLogo} 
									alt="club logo" className="club-media"/>
								<div className="d-flex flex-column club-card-body-text">
									<p>{scholarship.clubId.userId.firstName}</p>
									<p>{scholarship.clubId.clubAddress}</p>
								</div>
							</CardBody>
						</Card>
					)
				})}
			</Row>
		</div>
	)
}

export default ScholarshipAll;